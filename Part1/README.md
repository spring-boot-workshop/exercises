## Aufgabe 1 - Erste Spring App

Erstelle deine eigene Spring-App. Analog zum Beispiel soll ein 
REST-Endpunkt angelegt werden, der folgende API unterstützt:

`/time`: Liefert die aktuelle Zeit als String zurück.

Beispiel: “2020-01-07T13:56:10”

Der Endpunkt soll folgende Query Parameter unterstützen:

`/time?only=time`: Liefert nur die aktuelle Zeit ohne Datum zurück. 

Beispiel: “13:56:10”

`/time?only=date`: Liefert nur das aktuelle Datum zurück.

Beispiel: “2020-01-07”
