## Docker Compose

In diesem Abschnitt erweiterst du das Shopping-Backend, damit jeder Service als Docker Container
innerhalb einer Docker-Compose Datei gestartet werden kann.

## Aufgabe 30 - Microservices starten

Service: Shopping-Cart-Service

Commit: `exercise: add eureka-client`

Analog zu Aufgabe 28 sollst du in dieser Aufgabe den Eureka-Client im Shopping-Cart-Service einbauen.
Der Client wird benötigt, damit sich der Service später bei der zentralen Eureka-Registry anmelden kann.
Die notwendingen Schritte sind in Aufgabe 28 beschrieben.

## Aufgabe 30.1 - Eureka zur Hostname-Auflösung nutzen

Service: API-Gatway

Commit: `exercise: use eureka-server`

Der Product-Service, das API-Gateway und der Shopping-Cart-Service melden sich nun bereits
beim Eureka-Server an. In dieser Aufgabe sollst du das API-Gateway erweitern, sodass die FeignClients
für den Product-Service und den Shopping-Cart-Service die URLs für die Services 
über die Eureka-Service-Registry ermitteln. 
Entferne dazu die URLs aus den beiden Klasse `ProductClient` und `ShoppingCartClient`.
Achtung: Diese Änderungen werden Einfluss auf die API Tests haben!

## Aufgabe 30.2 - Docker-Compose Netzwerk

Service: API-Gateway

Commit: `exercise: add docker-compose`

Nachdem nun alle Services als Docker Image verfügbar sind, kannst du eine Docker-Compose Datei erstellen,
in der die folgenden Services als Container hochgefahren werden sollen:

- Service-Registry
- API-Gateway
- Product-Service
- Shopping-Cart-Service
- Product-Importer
- Rabbit-MQ

Erstelle dafür einen Ordner docker im Root-Verzeichnis des API-Gateways. Dort erstellst du die docker-compose Datei.
Denk daran, dass du wahrscheinlich Konfiguration Properties einzelner Services anpassen musst.
Tipp: Dies lässt sich über Environment Variablen realisieren.


# Aufgabe 31 - API Proxy Zuul

## Aufgabe 31.1 - Route

Service: API-Proxy

Commit: `exercise: add route config`

In dieser Aufgabe lernst du denn den API-Proxy [Zuul](https://github.com/Netflix/zuul/wiki) kennen. 
Der API-Proxy soll Client-Anfragen entgegennehmen und an das API-Gateway weiterleiten.

Richte dazu eine Route ein, die Anfragen an die URL `/api-gateway/products` an das API-Gateway als Anfrage `/products` weiterleitet.

Die notwendigen Schritte findest du in der [Dokumentation](https://cloud.spring.io/spring-cloud-netflix/multi/multi__router_and_filter_zuul.html)

## Aufgabe 31.2 - Actuator

Service; API-Proxy

Commit: `exercise: enable zuul actuator`

Zuul stellt zwei eigene Actuator Endpunkte bereit: `routes` und `filters`.
Aktiviere diese in der `application.yml`. 

Starte anschließen den Proxy und schaue dir den Inhalt unter folgender URL an:

    http://localhost:8085/actuator/routes
    
## Aufgabe 31.3 - Filter

Service: API-Proxy

Commit: `exercise: add custom header filter`

Zuul bietet die Möglichkeit per Filter Einfluss auf einen Request oder eine Response zu nehmen.
In dieser Aufgabe sollst du einen Filter implementieren, der jeder Response folgenden Header hinzufügt:

    X-Powered-by: zuul
    
Starte zuerst mit einem Test in der Klasse `RoutingTest`. 

# Aufgabe 32 - Centralized Configuration

Service: API-Gateway

Commit: `exercise: add config client`

In dieser Aufgabe soll das API-Gateway erweitert werden, damit es einige Konfigurationsparameter
von einem zentralen Config-Server beziehen kann. 

## Aufgabe 32.1 - Version Endpunkt

Erstelle zuererst einen neuen Endpunkt `GET /version`. Dieser soll bei die aktuelle Version der genutzten Konfiration
als String in folgendem Format zurückgeben:

    "Current config version: 1.0"

Starte dazu mit einem Test in der Klasse `ConfigVersionControllerTest`. Der Controller soll die aktuelle Version
über eine Property in der `application.yml` einlesen. Verwende hierzu folgendes Format:

    config:
        version: 1.0
        
## Aufgabe 32.2 - Config Server

Service: Config-Server

Commit: `exercise: add config server`

Spring Cloud ermöglicht es einen zentralen Config Server zu nutzen, von dem einzelne Services
ihre Konfiguration beziehen können. In dieser Aufgabe sollst du einen Config-Server implementieren,
der die Konfiguration für das API-Gateway bereitstellt.

Die notwendigen Schritte findest du in der [Dokumentation](https://cloud.spring.io/spring-cloud-config/multi/multi__spring_cloud_config_client.html)

Die Konfiguration soll von folgendem Git-Repository geladen werden:

    https://gitlab.com/spring-boot-workshop/configuration/config-storage.git
    
Nachdem du den Server implementiert hast, kannst du die Konfiguration für das API-Gateway unter folgender URL abrufen:

    http://localhost:8889/api-gateway/default
    
## Aufgabe 32.3 - Config Client

Service: API-Gateway

Commit: `exercise: add config client`

In dieser Aufgabe soll dein Stant aus Aufgabe 32.1 erweitert werden, sodass das Property `config.version` vom Config Server bezogen werden soll.

Die notwendigen Schritte findest du in der [Dokumentation](https://cloud.spring.io/spring-cloud-config/multi/multi__spring_cloud_config_client.html)

Lege dazu die notwendige `bootstrap.yaml` Datei an, in der du die URL des Config-Servers festlegen kannst. Als Dependency benötigst du noch:

    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-config</artifactId>
        <version>2.2.1.RELEASE</version>
    </dependency>

Starte nun zuerst den Config-Server und danach das API-Gateway. Ein Aufruf der folgenden URL sollte die Version 1.2 zurückliefern:

    http://localhost:8080/version
