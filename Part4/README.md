## Spring Security

## Aufgabe 18 - Importer Endpunkte absichern

Service: Product-Importer-Service

Commit: `exercise: add security config`

In dieser Aufgabe sollst du die Endpunkte im Product-Importer-Service mittels Basic Authentication absichern. 
Hierzu genügt es im ersten Schritt folgende Dependencies einzubinden:

```java
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-security</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.security</groupId>
    <artifactId>spring-security-test</artifactId>
</dependency>
```

Die Abhängigkeit Starter Security sichert alle Endpunkte standardmäßig mittels Basic Authentication ab. 
Es wird ein Standard User konfiguriert mit folgenden Zugangsdaten:

    Username: user
    Password: automatisch generiert (wird beim Starten in der Konsole angezeigt)
    
Im nächsten Schritt soll diese Standardkonfiguration individualisiert werden. Hierzu legst du eine Klasse `SecurityConfig`
im Package `security` an:

```java 
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // add your http security config here
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        // add your user accounts here
    }
}
```

Erstelle zunächst zwei neue User mit folgenden Zugangsdaten:

    // user
    Username: user
    Password: password
    Rolle: USER
    
    // admin
    Username: admin
    Password: admin
    Rolle: ADMIN
 
Der Entpunkt `DELETE /imports/products` soll nur noch für User mit der Rolle ADMIN erlaubt werden. 

### Aufgabe 18.1 - Security Tests

Nach der Absicherung der Endpunkte musst du die `ImportControllerTests` anpassen, 
da diese ohne Authentifizierung nicht mehr funktionieren. Hierfür bietet sich die Annotation `@WithMockUser` an.
Zusätzlich zu den bestehenden Tests, solltest du auch die neuen Fälle prüfen:
    
1. Der Zugriff auf `POST /imports/products` sollte ohne User nicht möglich sein.
2. Der Zugriff auf `DELETE /imports/products` sollte ohne User nicht möglich sein.
3. Der Zugriff auf `DELETE /imports/products` sollte mit der Rolle USER nicht möglich sein.

## Aufgabe 19 - REST assured Test

Service: Product-Importer-Service

Commit: `exercise: add rest assured test`

Bisher wurde die Basic Authentifizierung in unseren Tests durch die Annotation `@WithMockUser` übernommen.
Nun sollst du einen neuen Test erstellen in der Klasse `EndpointTest` implementieren, der mittes Rest assured 
den Endpunkt `/imports/products` inkl. Authentifizierung aufruft. 

## Aufgabe 20 - HTTPS aktivieren

Service: Product-Importer-Service

Commit: `exercise: add HTTPS to service`

In dieser Aufgabe sollst du den Import-Endpunkt des Product-Importer-Service mittels HTTPS absichern.
Hierzu wird ein Zertifikat benötigt. Für die lokale Entwicklung genügt hier ein selbstsigniertes Zertifkat.

Mit folgendem Befehl kannst du einen Java-kompatiblen Keystore erstellen:

    keytool -genkeypair -alias spring-demo -keyalg RSA -keysize 2048 -storetype PKCS12 -keystore keystore.p12 -validity 3650 -storepass password

Diesen legst du im Resources Ordner des Product-Importer-Service ab. 
HTTPS lässt sich nun mittels der `application.properties` [aktivieren](https://docs.spring.io/spring-cloud-dataflow/docs/1.1.0.M1/reference/html/getting-started-security.html). 

Überprüfe ob nach der Aktivierung alle Tests noch funktionieren und passe eventuell fehlgeschlagene Tests entsprechend an.

## Building Docker Images 

## Aufgabe 21 - Docker Build mit Dockerfile
Service: Shopping-Cart-Service

Commit: `exercise: add docker build via Dockerfile` 

In dieser Aufgabe sollst du das Fat-Jar des Shopping-Cart-Services in ein lauffähiges Docker Image verpacken.
Hierzu musst du ein passendes `Dockerfile` im Root-Verzeichnis des Service anlegen. Das Image kannst du anschließend
mit folgendem Befehl erstellen:

    docker build -t spring-demo/shopping-cart-service:latest .

## Aufgabe 22 - Docker Build mit Google Jib
Service: Shopping-Cart-Service

Commit: `exercise: add google jib`

Statt ein eigenes Dockerfile zu erstellen und das Docker Image manuell mit `docker build` zu erstellen, 
gibt es einige Alternativen, die das Erstellen von Docker Images per Maven Plugin ermöglichen.

Zu diesen Alternativen zählen u.a.: [fabric8io](https://github.com/fabric8io/docker-maven-plugin)
und [Google Jib](https://github.com/GoogleContainerTools/jib). 
Zweiteres unterstütz zusätzlich das Erstellen von Docker Images ohne Docker Daemon. 
Dies kann besonders in CI-Umgebungen sehr hilfreich sein.

In dieser Aufgabe sollst du dein bisheriges Dockerfile durch das 
[Googel Jib Maven Plugin](https://github.com/GoogleContainerTools/jib/tree/master/jib-maven-plugin)
ersetzen.

Erstelle ein Image mit dem Namen `spring-demo/shopping-cart-service` mit dem Tag `latest`. 
Das Image soll im Maven Lifecycle `package` erstellt werden.

## Aufgabe 23 - Googel Jib everywhere
In dieser Aufgabe sollst du das Google Jib Plugin in die übrigen Services integrieren. Erweitere dazu die jeweilige `pom.xml` 
der folgenden Services:

- Product-Service (commit: `exercise: add docker build via Google Jib`)
- API-Gateway (commit: `exercise: add docker build via Google Jib`)

## Aufgabe 24 - Ende-zu-Ende Test mit Testcontainers
In dieser Aufgabe machst du dir das Image des Product-Service zu nutze, um einen Ende-zu-Ende Test im API-Gateway zu implementieren.
Hierzu erstellst du eine neue Test-Klasse `ApiEndpointContainerTest` im Package `e2e`.
Dieser Test soll den gesamten `Application Context` starten und zusätzlich den Product-Service als Docker Container.
Docker Container lassen sich über das Projekt [Testcontainers](https://www.testcontainers.org/features/creating_container/) einbinden.
Die Einbindung in Junit5 Tests ist ebenfalls sehr gut [dokumentiert](https://www.testcontainers.org/test_framework_integration/junit_5/).

Implementiere folgenden Test im Kontext eines Ende-zu-Ende Tests:

```java 
@Test
void shouldReturnProducts() {

    when()
            .get("/products")
            .then()
            .body("size()", is(4));
}
```

## Circuit Breaker

## Aufgabe 25 - Circuit Breaker

Service: API-Gateway

Commit: `exercise: implement a fallback method as a circuit-breaker`

In dieser Aufgabe soll die Methode `getProducts()` in der Klasse `ProductService` um eine Fallback Methode erweitert werden.
Überlege dir zu Beginn welche Testklasse du nutzen kannst, um die Funktionalität zu testen. 

Tipp: Du wirst den Spring Kontext brauchen. Wiremock bietet eine Funktion um die Antwort zu verzögern.

Die Fallback Methode soll folgende Liste zurück geben:

    List.of(new Product(1, "invalid", 0));
    
Die notwendigen Schritte findest du in der [Dokumentation](https://cloud.spring.io/spring-cloud-netflix/multi/multi__circuit_breaker_hystrix_clients.html)

## Service Discovery

Deine Aufgabe ist es, eine Eureka Service Registry App zu erstellen. 
In dieser Service Registry werden sich dann unser Product-Service, sowie das API-Gateway registrieren.

## Aufgabe 26 - Eureka Server

Service: Service-Registry

Commit: `exercise: add eureka-server`

Los geht es wie immer auf dem Exercise Branch. Deine Aufgabe ist es die bestehende Klasse ServiceDiscoveryApplication zu erweitern,
sodass die App eine Eureka Service Registry startet. 

Die notwendigen Schritte findest du in der [Dokumentation](https://cloud.spring.io/spring-cloud-netflix/reference/html/#spring-cloud-eureka-server).

Wenn du fertig bist, sollte deine Applikation unter folgender URL verfügbar sein:
    
    http://localhost:8084/

### Aufgabe 27 - Eureka Client

Service: Product-Service

Commit: `exercise: exercie: add eureka client`

Das API-Gateway nutzt die hardcodierten URLs für den Product-Service und den Shopping-Cart-Service. 
Die URL für den Product-Service soll durch die Eureka Service Registry aufgelöst werden. 

Erweitere hierzu als erstes den Product-Service um einen Eureka-Client, 
mit dem dieser sich an der Ereka Service Registry registriert. 
Die notwendigen Schritte findest du in der [Dokumentation](https://cloud.spring.io/spring-cloud-netflix/reference/html/#registering-with-eureka).

Wenn du fertig bist solltest du den Product-Service als neue Applikation in der Eureka Service Registry sehen:

![eureka-registry](img/eureka-registry.png) 

## Aufgabe 28 - Eureka Client nutzen

Service: API-Gateway

Commit: `exercise: add eureka client`

Als nächstes gilt die URL des Product-Service im API-Gateway durch den Service-Name des Product-Service zu ersetzen.
Entferne dazu den URL-Parameter aus der `@FeignClient` Annotation. 

Die notwendigen Schritte findest du in der [Dokumentation](https://cloud.spring.io/spring-cloud-netflix/multi/multi_spring-cloud-feign.html).

# Reactive Programming

## Aufgabe 29 - Reactive Programming

Service: Product-Service

Commit: `exercise: reactive programming`

In dieser Aufgabe sollst du erste Schritte in der reaktiven Programmierung mit Spring unternehmen. 
Implementiere hierzu die folgenden Endpunkte in der Klasse `ReactiveProductController`:
    
    GET /stream/products
    
Zusätzlich soll ein weitere Endpunkt implementiert werden, 
der die Elemente in der Produkt-Liste mit einer Verzögerung von einer Sekunde zurückliefert.
    
    GET /stream/delay/products
