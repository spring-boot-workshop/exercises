## Datenbanken einbinden

Service: Product-Service

Commit: `add product repository and REST endpoint`

In dieser Aufgabe sollst du den Product-Service entwickeln. Dieser stellt eine REST-Schnittstelle bereit, um Produkte abzufragen und zu speichern, sowie Preise abzurufen.
Du wirst den Service in den folgenden Aufgaben Schicht für Schicht entwickeln, angefangen mit der äußersten REST-Schnittstelle, bis hin zur Datenbank-Schicht.

### Aufgabe 2 - REST-Schnittstelle implementieren

In dieser Aufgabe sollst du die REST-Schnittstelle des Product-Service entwickeln. Die Schnittstelle soll folgende Funktionen ermöglichen:

```
// liefert alle gespeicherten Produkte zurück
GET /products 
    
// speichert ein neues Produkt, das per Request-Body gesendet wird und liefert dieses zurück.
POST /products

// liefert das Produkt mit der id=id zurück
GET /products/{id}
    
// liefert den Preis des Produkts mit der id=id zurück
GET /products/{id}/prices   
```
    
Beginne mit den entsprechenden Tests für die REST-Schnittstelle, um dann schrittweise den `ProductService` zu entwickeln. Am Ende dieser Aufgabe soll die REST-Schnittstelle, sowie die Klasse `ProductService` implementiert sein.

## Aufgabe 3 - Datenbank mittels Repository anbinden

In dieser Aufgabe sollst du die Datenbank anbinden, in der die Produkte gespeichert werden können. Neben den für den benötigten Funktionen für
den `ProductService` sollen noch weitere Methoden implementiert werden. Das `ProductRepository` soll neben den Standard
CRUD Methoden noch folgende Methode bereitstellen:

```java
long countByName(String productName);
```

Beginne auch hier mit den Testfällen für die einzelnen Repository Methoden. Danach kannst du das Repository in der Klasse `ProductService` nutzen.

### Aufgabe 3.1

Funktioniert deine Spring App bereits? Starte die Anwendung für einen Test mit Postman. Hierfür wirst du
für deinen Produktiv-Code auch die h2 Datenbank nutzen müssen.

## Aufgabe 4 - Caching mit Spring

Service: Product-Service

Commit: `exercise: add caching to service`

In dieser Aufgabe sollst du einen Cache für die Methode `public Iterable<Product> findAll()` im Product Service hinzufügen.
Spring bietet bereits eine eingebaute Untersützung für Caching. Mittels der `@Cachable` Annotation kann ein Methodenaufruf gecached werden.

Starte zuerst mit einem geeigneten Test, um zu überprüfen wie oft die Repository Methode `findAll()` aufgerufen wird. 
Denke daran, dass dein Cache eventuell auch wieder geleert werden muss. Weitere Details sind in der [Dokumentation](https://docs.spring.io/spring/docs/4.1.x/spring-framework-reference/html/cache.html)
zu finden.

## Aufgabe 5 - Datenbank Management mit Flyway

Service: Product-Service

Commit: `exercise: add flyway script`

In dieser Aufgabe erzeugst du das Datenbankschema und befüllst die Datenbank mit initialen Daten mittels Flyway.
Die notwendigen Schritte zur Einbindung von Flyway sind in der [Dokumentation](https://flywaydb.org/documentation/plugins/springboot)
beschrieben. 

Datenbank initializierungen lassen sich bequem per SQL-Skript ausführen. Die notwenigen Schritte sind in der 
[Flyway Dokumentation](https://flywaydb.org/documentation/migrations#discovery) beschrieben.

Folgende Daten sollen in die Datenbank beim Starten des Services importiert werden:

```
INSERT INTO product (name, price)
VALUES ('iPhone', 999),
       ('iPad', 399),
       ('Mouse', 29),
       ('Keyboard', 140)
```

Das benötigte Datenbankschema für unsere Product-Klasse lässt sich mit folgendem Skript erstellen:
```
create table if not exists product
(
    id    bigint            not null auto_increment primary key,
    name  varchar(255)      not null,
    price double precision  not null
);
```

## Aufgabe 6 - Stage spezifische Konfiruation

Service: Product-Service

Commit: `exercise: add profile for external db`

In dieser Aufgabe soll eine Spring Konfiguration angelegt werden, um den Product-Service mittels einer externen Datenbank zu nutzen.
Dazu benötigst du im ersten Schritt eine MariaDB Instanz, die du mit folgender Docker-Compose Datei (`docker-compose.yml`) erzeugen kannst:

```
// docker-compose.yml
version: '3.1'
services:
  db:
    image: mariadb
    container_name: mariadb
    ports:
      - 3306:3306
    environment:
      MYSQL_DATABASE: db
      MYSQL_USER: user
      MYSQL_PASSWORD: password
      MYSQL_ROOT_PASSWORD: password
```

Die Datebank kannst du nun über Docker-Compose starten:

  docker-compose up

Lege als nächstes eine `application-db.yaml` Datei unter `src/main/resources` an. In dieser Datei musst du nun die Zugangsdaten 
und die URL für die externe Datenbank konfigurieren. Die notwendigen Details sind in der 
[Dokumentation](https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html#boot-features-connect-to-production-database)
beschrieben.

Als manueller Test solltest du deine App jetzt starten können und deine Daten werden in der externen Datenbank persitiert.
