## Aufgabe 7 - REST Endpunkte

Service: Shopping-Cart-Service

Commit: `exercise: add shipping cart endpoint`

In dieser Aufgabe soll der Shopping-Cart-Service implementiert werden. 
Checke hierzu als erstes den [Shopping-Cart-Service](https://gitlab.com/spring-boot-workshop/shopping-cart-service) als neues Projekt aus.

Der Service soll einen REST-Endpunkt mit der
folgenden Schnittstellen-Definition erhalten:

``` 
// erstellt ein neues Shopping-Cart Objekt und liefert dieses zurück
POST /shoppingcarts    

// gibt das Shopping-Cart Objekt mit der id=id zurück                             
GET /shoppingcarts{id}           

// fügt die Product ID (product_id) dem Shopping-Cart Objekt mit der id=id hinzu.                       
POST /shoppingcarts/{id}/products/{product_id}

// löscht falls vorhanden die Product ID (product_id) aus dem Shopping-Cart Objekt mit der id=id.          
DELETE /shoppingcarts/{id}/products/{product_id}        
```

Starte mit einem einer neuen Test-Klasse `ShoppingCartControllerTest`, um den Web-Layer zu testen. Der Controller soll
die HTTP-Anfragen entgegennehmen und an einen ShoppingCartService weiterleiten. Diesen solltest du bereits als `@MockBean`
vorsehen.

### Aufgabe 7.1 - JSON-Konvertierung steuern

Service: Shopping-Cart-Service

Commit: `exercise: ignore internal property`

Die Klasse `ShoppingCart` hat ein Attribut mit dem Namen `internalId`. Dieses Attribut sollte nicht mit über die REST-Schnittstelle
ausgegeben werden. Aktuell ist dies jedoch der Fall:

```json
{
    "id": 1,
    "products": [],
    "internalId": 0.0020324309847771227
}
```

Deine Aufgabe ist es nun, dieses Attribut aus der Schnittstellen-Antwort zu entfernen. Die Antwort soll dann so aussehen:

```json
{
    "id": 1,
    "products": [],
}
```

## Aufgabe 8 - Error Handling

Service: Product-Service

Commit: `exercise: add error handling`

In dieser Aufgabe sollst du den Product-Service REST-Endpunkt um eine eigene Error-Response in folgendem Schema erweitern:

```
{
    "statusCode": 404,
    "message": "Product was not found"
}
```

Diese Fehlermeldung soll zurück geliefert werden, falls das angefragte Produkt nicht in der Datenbank existiert. 


## Aufgabe 9 - Deklarative REST Clients

Service: API-Gateway

Commit: `exercise: add rest clients`

In dieser Aufgabe sollst du das API-Gateway mit dem Product-Service, sowie dem Shopping-Cart-Service verbinden. 
Checke als erstes das [API-Gateway](https://gitlab.com/spring-boot-workshop/api-gateway) als neues Projekt aus.
Das API-Gateway stellt über seine REST-Schnittstelle die notwendigen Funktionalitäten für z.B. einen Web-Client bereit.

Die beiden Services sollen über die `ProductRepository` und die `ShoppingCartRepository` Klasse angebunden werden. Nutze hierfür
die Möglichkeit die REST-Clients über Feign zu realisieren. 
Die notwendigen Schritte dazu findest du im in der [Dokumentation](https://cloud.spring.io/spring-cloud-netflix/multi/multi_spring-cloud-feign.html).

Die URLs der Services sollen sich über Properties konfigurieren lassen. Überlege dir, wie die REST-Clients getestet werden können.
Statische Daten sind unter `src/test/resources/__files` verfügbar. 

## Aufgabe 10 - Ende-zu-Ende Test

Service: API-Gateway

Commit: `exercise: add wiremock endpoint test`

Nachdem die REST-Clients implementiert wurden, bietet sich ein Ende-zu-Ende Test an, um alle Komponenten von der REST-Schnittstelle
bis zu den REST-Clients zu testen. Deine Aufgabe ist es diesen Test in der Klasse `ApiEndpointWiremockTest` mittels Wiremock und REST assured zu implementieren.
Der Produktionscode sollte keine Änderung mehr erfordern.

## Aufgabe 11 - OpenAPI Integration

Service: API-Gateway

Commit: `exercise: add swagger-ui`

In dieser Aufgabe sollst du das API-Gateway um eine OpenAPI Dokumentation ergänzen. Dies ist mit mehreren externen Abhängikeiten 
möglich. OpenAPI v3 wird aktuell nur von [springdoc-openapi](https://github.com/springdoc/springdoc-openapi) unterstützt.
Diese Projekt wird allerdings nicht vom Spring Team maintained. Die notwendigen Schritte sind gut dokumentiert.

Beginne mit einem `@SpringBootTest`, der einen Endpunkt unter folgender URL erwartet:

    http://localhost:8080/swagger-ui/index.html
    
Über diese URL kannst du die Swagger-UI anschließend erreichen. Dort wird das OpenAPI JSON übersichtlich dargstellt.

## Spring Apps monitoren

## Aufgabe 12 - Actuator Endpunkt

Service: API-Gateway

Commit: `exercise: add actuator`

In dieser Aufgabe sollst du das API-Gateway um einen sogenannten Actuator `/actuator` erweitern. 
Beginne mit einer neuen Testklasse `ActuatorEndpointTest` im Package `rest`.

Wenn du fertig bist kannst du die verfügbaren Endpunkte unter folgender URL z.b. mit Postman aufrufen:

    http://localhost:8080/actuator


## Aufgabe 13 - Prometheus & Grafana

Service: API-Gateway

Commit: `exercise: add prometheus`

Lege einen neuen Ordner `prometheus` im Root-Verzeichnis des API-Gateways an. In diesem Ordner legst du folgende beide Dateien an:

```docker
# docker-compose.yml
version: '3.7'
services:
  prometheus:
    image: prom/prometheus:v2.15.1
    container_name: prom
    ports:
      - 9090:9090
    volumes:
      - ./prometheus.yml:/etc/prometheus/prometheus.yml
  grafana:
    image: grafana/grafana:6.5.2
    ports:
      - 3000:3000
    environment:
      - GF_SECURITY_ADMIN_USER=admin
      - GF_SECURITY_ADMIN_PASSWORD=password
```

```
# prometheus.yml
scrape_configs:
  - job_name: 'prometheus'
    scrape_interval: 5s
    static_configs:
      - targets: ['localhost:9090']
  - job_name: 'api-gateway'
    scrape_interval: 5s
    metrics_path: '/actuator/prometheus'
    static_configs:
      - targets: ['host.docker.internal:8080']
```

Anschließend kannst du Prometheus und Grafana starten. Die Prometheus Web-Oberfläche ist unter folgender URL erreichbar:

    http://localhost:9090
    
Grafana erreischt du unter folgender URL:

    http://localhost:3000
    
Starte nun die API-Gateway Applikation, um die Daten durch Prometheus abholen zu lassen.

## Aufgabe 14 - Actuator Endpunkt isolieren

Service: API-Gateway

Commit: `exercise: change mangement port`

Aktuell ist der Actuator Endpunkt und der Prometheus Endpunkt über den selben Port wie die produktiven Endpunkte verfügbar. 
Da über diese Endpunkte sensible Informationen abgefragt werden können, sollten diese Endpunkte über einen gesonderten Port 
erreichbar gemacht werden, der z.B. von außen nicht erreichbar ist.

Der Post lässt sich über eine Spring Property ändern. Ändere den Port auf 9080 und beachte die notwendigen Änderungen in
der Prometheus Config.

Pürfe nun nun ob die Werte weiterhin in Grafana verfügar sind.

## Aufgabe 15 - Eigene Metriken hinzufügen

Service: API-Gateway

Commit: `exercise: add custom metric`

Neben denn Standard Metriken, die der Prometheus-Endpunkt `actuator/prometheus` bereitstellt, kann es nützlich sein, eigene Metriken hinzuzufügen.
In dieser Aufgabe sollst du eine Metrik mit dem Namen `http_client_request_products` hinzufügen. Diese Metrik soll die benötigte Zeit zum Aufruf des Product-Service messen.

Hierzu wirst du die Spring AOP Starter Abhängigkeit benötigen:

``` 
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-aop</artifactId>
</dependency>
```

Erweitere zu Beginn den `PrometheusEndpointTest` um einen weiteren Testfall, der sicherstellt, dass die neue Metrik über den Prometheus-Endpunkt verfügbar ist.
Beachte, dass Metriken erst verfügbar sind, sobald der erste Wert gemessen wurde. 

## Messaging mit Spring

## Aufgabe 16 - Message Sender

Service: Product-Importer-Service

Commit: `exercise: add message sender`

In dieser Aufgabe soll der Product-Importer-Service um die Klasse `MessageSender` erweitert werden.
Checke als erstes den neuen Service [Product-Importer-Service](https://gitlab.com/spring-boot-workshop/product-importer)
Diese Klasse soll ein `Product` an eine RabbitMQ Queue senden. In einer späteren Aufgabe wirst du dann einen Receiver im
Product-Service implementieren. 

Bevor du mit der Implementierung beginnst, benötigst du eine laufende RabbitMQ-Instanz, um diese in den Tests anzusprechen.
Hierzu kannst du folgende Docker-Compose Datei nutzen:

``` 
version: '3.3'
services:
  rabbitmq:
    container_name: rabbitmq
    image: rabbitmq:3-management
    ports:
      - "5672:5672"
      - "15672:15672"
```

Lege diese im Root-Verzeichnis des Product-Importer-Service an.

Die RabbitMQ Weboberfläche erreichst du unter folgender URL:

    http://localhost:15672
    
Die Zugangsdaten lauten:

    Username: guest
    Password: guest

### Aufgabe 16.1 - RabbitMQ Mock

Service: Product-Importer-Service

Commit: `exercise: add rabbit-mq mock`

In dieser Aufgabe soll der Test `MessageSenderTest` auch ohne laufende RabbitMQ-Instanz lauffähig gemacht werden.
Hierzu bietet das Projekt [rabbitmq-mock](https://github.com/fridujo/rabbitmq-mock) eine sehr komfortable Spring Integration an.

## Aufgabe 17 - Message Receiver

Service: Product-Service

Commit: `exercise: add message receiver`

In dieser Aufgabe sollst du einen Message Receiver im Product-Service implementieren.
Dieser soll die Nachrichten des Product-Importer-Service empfangen können und die darin enthaltenden Produkte
in der Datenbank ablegen können. Hierzu kann die bereits vorhandenen Methode `save(Product product)` im `ProductService`
genutzt werden.
